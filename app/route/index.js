const mongoose = require('mongoose');
const to = require('await-to-js').default;
const router = require('express').Router();
const { generate, authMiddleware } = require('../../helpers/token');

const User = require('../models/User');
const Score = require('../models/Scores');

// New or existing user
router.post('/user', async (req, res, next) => {
  const { email, name } = req.body;
  let err, token;

  if (!email) {
    return res.status(500).json({ error: 1, message: 'Email was not found' });
  }

  [err, user] = await to(User.findOne({ email }));
  if (err) return res.status(500).json({ error: 1, message: err.message });

  if (!user) {
    const newUser = new User({ email, name });
    [err, saveUser] = await to(newUser.save());
    if (err) return res.status(500).json({ error: 1, message: err.message });

    token = generate({ _id: saveUser._id });
    return res.status(200).json({ error: 0, token });
  } else {
    token = generate({ _id: user._id });
    return res.status(200).json({ error: 0, token });
  }
});

// Store score
router.post('/score', authMiddleware, async (req, res, next) => {
  const userId = res.locals.decode._id;
  const { score, level } = req.body;

  const [err, saveScore] = await to(
    new Score({ score, level, user: mongoose.Types.ObjectId(userId) }).save()
  );
  if (err) return res.status(500).json({ error: 1, message: err.message });

  if (saveScore) {
    return res.status(201).json({ error: 0, message: 'Score saved!' });
  }
});

// Leaderboard
router.get('/leaderboard', authMiddleware, async (req, res, next) => {
  const userId = res.locals.decode._id;
  let err;

  const topTenAgg = [
    {
      $match: {
        user: mongoose.Types.ObjectId(userId),
      },
    },
    {
      $sort: {
        score: -1,
      },
    },
    {
      $limit: 10,
    },
    {
      $lookup: {
        from: 'users',
        localField: 'user',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
      $unwind: {
        path: '$user',
      },
    },
    {
      $project: {
        level: 1,
        score: 1,
        user: {
          name: '$user.name',
        },
      },
    },
  ];

  const statisticsAgg = [
    {
      $match: {
        user: mongoose.Types.ObjectId(userId),
      },
    },
    {
      $group: {
        _id: '$user',
        score: {
          $avg: '$score',
        },
        totalGames: {
          $sum: 1,
        },
        maxLevel: {
          $max: '$level',
        },
      },
    },
    {
      $project: {
        _id: 0,
        maxLevel: 1,
        score: 1,
        totalGames: 1,
      },
    },
  ];

  [err, topGames] = await to(Score.aggregate(topTenAgg));
  if (err) return res.status(500).json({ error: 1, message: err.message });

  [err, stats] = await to(Score.aggregate(statisticsAgg));
  if (err) return res.status(500).json({ error: 1, message: err.message });

  if (topGames && stats) {
    return res
      .status(200)
      .json({ error: 0, topTen: topGames, stats: stats[0] });
  }
});

module.exports = router;
