const { sign, verify } = require('jsonwebtoken');

const generateToken = (payload) => {
  return sign(payload, process.env.SECRET_KEY);
};

const verifyToken = async (token) => {
  const tk = token.split(' ')[1];
  try {
    const decode = await verify(tk, process.env.SECRET_KEY);
    return { status: true, payload: decode };
  } catch (err) {
    return { status: false, err: err };
  }
};

const authMiddleware = async (req, res, next) => {
  const token = req.headers.authorization;
  if (!token) {
    return res
      .status(502)
      .json({ error: 1, message: 'Token not available in header' });
  }

  const verifyStatus = await verifyToken(token);
  if (verifyStatus.status) {
    res.locals.decode = verifyStatus.payload;
  } else {
    return res
      .status(502)
      .json({ error: 1, message: verifyStatus.err.message });
  }

  next();
};

module.exports = {
  generate: generateToken,
  authMiddleware,
  verifyToken,
};
