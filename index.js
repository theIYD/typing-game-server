const express = require('express');
const cors = require('cors');
const app = express();

// Dotenv
require('dotenv').config();
require('colors');

// Database connection
require('./helpers/db');

// Routes
const routes = require('./app/route');

// CORS
app.use(cors());

// Parser
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));

// Use routes
app.use('/', routes);

// Home route
app.get('/', (_req, res) => {
  res.status(200).json({ message: 'Hello World' });
});

// Start the server
const PORT = process.env.PORT || 3500;
app.listen(
  PORT,
  console.log(
    `Server running in ${process.env.ENV || 'development'} mode on port ${PORT}`
      .brightYellow.underline.bold
  )
);

process.on('uncaughtException', function (err) {
  // handle the error safely
  console.log(err);
});

module.exports = app;
